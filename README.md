# README #

Using this project you can easily create a Pull Request in BitBucket from command line (Mac).

# How to install it?

1. Clone the project
2. Add the bin folder to $PATH (export PATH=$PATH:<BBPR_HOME>/bin)
3. Add your BitBucket user and password as environment variables

export BIT_BUCKET_USERNAME=my_username

export BIT_BUCKET_PASSWORD=my_password

After this, `bbpr` should be a valid executable command.

# How to setup my project?

Create a file named `.repo_data` in the root of your project and fill it using this format:

project_owner=<PROJ_OWNER>

repository=<REPO_NAME>

reviewers=<REV_1>,<REV_2>,...,<REV_N>

*Example of .repo_data content:*

project_owner=tangocode

repository=bbpr

reviewers=bwillis,jblack,jroberts

# How to create a PR?

After you setup your project, go to the root of your project and run this command:

`bbpr <source_branch> <dest_branch> <title> <description> [<close_branch>]`

By default, the branch will be closed after merging the PR, unless <close_branch>, which is optional, is set to false.

*Example of command that creates a PR:*

`bbpr MyNewBranch develop "Cool feature" "Includes the code of a cool feature" false`

If the result is a big JSON, it means it succeeded.